public class AlgorithmSelector {

    private double min;
    private double max;
    private int selector;

    public AlgorithmSelector(int selector) {
        this.selector = selector;
        algorithmSelector();
    }

    public void algorithmSelector() {
        if (selector == 1) {
            min = -510;
            max = 510;
        } else if (selector == 2) {
            min = -2.048;
            max = 2.048;
        } else if (selector == 3){
            min = -5.12;
            max = 5.12;
        } else {
            min = -5.12;
            max = 5.12;
        }
    }

    // -510 < x < 510
    public double schwefelFunction(double[] chromosome)  {
        double fitness=0.0;
        for (int i = 0; i < chromosome.length - 1; i++) {
            fitness +=(((-1)*chromosome[i]) * Math.sin(Math.sqrt(Math.abs(chromosome[i]))));
        }
        return fitness + (418.982887 * chromosome.length);
    }

    // -2.048 < x < 2.048
    public double rosenbrockFunction(double[] chromosome){
        double fitness = 0.0;
        for (int i = 0; i < chromosome.length - 1; i++) {
            fitness += 100 * Math.pow( (chromosome[i + 1])  - Math.pow(chromosome[i], 2), 2) + Math.pow((chromosome[i]) - 1, 2);
        }
        return fitness;
    }

    // -5.12 < x < 5.12
    public double rastriginFunction(double[] chromosome) {
        double fitness = 0;
        for (int i = 0; i < chromosome.length; i++) {
            fitness += (chromosome[i] * chromosome[i]) - (10 * Math.cos(2 * Math.PI * chromosome[i]));
        }
        fitness = (10 * chromosome.length) + fitness;
        return fitness;
    }

    // -5.12 < x < 5.12
    public double simpleFunction(double[] chromosome) {
        double fitness = 0;
        for(int i = 0; i < chromosome.length; i++) {
            fitness += ((1 + i) * (chromosome[i] * chromosome[i]));
        }
        return fitness;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public int getSelector() {
        return selector;
    }

    public void setSelector(int selector) {
        this.selector = selector;
    }
}
